let Router = require('express').Router();

Router.get('/', function(req, res){
    res.json({
        estado: 'Trabajando',
        mensaje: 'Este es el Router API!'
    });
});

// Importar  controller producto
var productoController = require('./productoController');

// Producto routes
Router.route('/productos')
    .get(productoController.index)
    .post(productoController.new);
    
Router.route('/productos/:producto_id')
    .get(productoController.view)
    .put(productoController.update)
    .delete(productoController.delete);

module.exports = Router;