var mongoose = require('mongoose');

// Setup schema
var productoSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    },
    valor: Number,
    add_date: {
        type: Date,
        default: Date.now
    }
});

// Exportar modelo producto 
var Producto = module.exports = mongoose.model('producto', productoSchema);

module.exports.get = function (callback, limit) {
    Producto.find(callback).limit(limit);
}