// Importar model producto
Producto = require('./productoModel');

// Buscando un producto
exports.index = function (req, res) {
    Producto.get(function (err, productos) {
        if (err) {
            res.json({
                estado: "error",
                mensaje: err,
            });
        }
        res.json({
            estado: "OK",
            mensaje: "Producto obtenido exitosamente.",
            data: productos
        });
    });
};

// crear producto
exports.new = function (req, res) {
    var producto = new Producto();
    producto.nombre = req.body.nombre ? req.body.nombre : producto.nombre;
    producto.descripcion = req.body.descripcion;
    producto.valor = req.body.valor;

    // guardar producto
    producto.save(function (err) {
        res.json({
            message: 'Nuevo producto creado!',
            data: producto
        });
    });
};

// ver info producto
exports.view = function (req, res) {
    Producto.findById(req.params.producto_id, function (err, producto) {
        if (err)
            res.send(err);
        res.json({
            mensaje: '1 producto encontrado!',
            data: producto
        });
    });
};

// Actualizar info producto
exports.update = function (req, res) {
    Producto.findById(req.params.producto_id, function (err, producto) {
        if (err)
            res.send(err);
            producto.nombre = req.body.nombre ? req.body.nombre : producto.nombre;
        producto.descripcion = req.body.descripcion;
        producto.valor = req.body.valor;

        // actualizar producto
        producto.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                mensaje: 'Informacion actualizada de producto',
                data: producto
            });
        });
    });
};

// borrar producto
exports.delete = (req, res) => {
    console.log('del 1');
    const prod = Producto.deleteOne({ _id: req.params.producto_id }, (err, producto) => {
        console.log('del 2');
        if (err) res.send(err);
        res.json({ estado: "exitoso", mensaje: 'Producto borrado' });
    });
    if(!prod) {
        res.status(400).send("Producto no encontrado");
    }
    res.send(prod)
};
