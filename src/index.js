let express    = require('express'),
    mongoose   = require('mongoose'), 
    bodyParser = require('body-parser');

let app = express();

let apiRoutes = require('./routes/producto.routes');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

mongoose.connect("mongodb+srv://user_temp:simplesimple@cluster0-5yxag.gcp.mongodb.net/productosDB?retryWrites=true&w=majorit", {useNewUrlParser: true, useUnifiedTopology: true});
var db = mongoose.connection;

const PORT = process.env.PORT || 3000;

app.get('/', function(req, res){
    res.send("Express is running successfully!");
});

app.use('/api', apiRoutes);

app.listen(PORT, function () {
    console.log("Servidor iniciado en puerto " + PORT);
});