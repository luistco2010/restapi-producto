FROM node:10-alpine

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./

USER node

ENV PORT=3000

RUN npm install

ADD --chown=node:node . .

EXPOSE $PORT

CMD [ "npm", "run", "start" ]